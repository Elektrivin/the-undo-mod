package me.elektrivin.undomod.reference;

import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;


public class Position {
    private BlockPos position;

    private World world;

    public Position(BlockPos position, World world) {
        this.position = position;
        this.world = world;
    }

    public Position(Entity entity) {
        this.position = entity.getPosition();
        this.world = entity.getEntityWorld();
    }

    public BlockPos getPosition() {
        return position;
    }

    public void setPosition(BlockPos position) {
        this.position = position;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public double getDistance(Position position) {
        return this.position.getDistance(position.getPosition().getX(), position.getPosition().getY(), position.getPosition().getZ());
    }

    @Override
    public String toString() {
        return "x: " + position.getX() + ", y: " + position.getY() + ", z: " + position.getZ() + " in world: " + world.getWorldInfo().getWorldName();
    }
}
