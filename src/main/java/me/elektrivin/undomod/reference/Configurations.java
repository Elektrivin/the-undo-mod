package me.elektrivin.undomod.reference;

import me.elektrivin.undomod.reference.ModInfo;
import net.minecraftforge.common.config.Config;

@Config(modid = ModInfo.MOD_ID)
public class Configurations {

    @Config.Name("Undo and Redo Configurations")
    @Config.Comment("All configurations for undo and redo features.")
    public static UndoRedo undoRedo = new UndoRedo();

    public static class UndoRedo {

        @Config.RangeInt(min = 0, max = 1000)
        @Config.Name("History Stack Size")
        @Config.Comment("This setting determines the maximum number of allowed undo and redo operations.")
        public int historyStackSize = 10;

        @Config.Name("Drop Refund")
        @Config.Comment({"Should items be dropped on refund?", "False if the item should be inserted in the players inventory or true if the item should get dropped."})
        public boolean dropRefund = false;

        @Config.Name("Enable Undo/Redo Range")
        @Config.Comment("Determines if there should be a maximum range the player has to be in for performing undo and redo operations.")
        public boolean enableRange = true;

        @Config.Name("Undo/Redo Range")
        @Config.Comment("Specifies the maximum distance between the player and the subjected undo or redo location")
        public double range = 15.0;

    }
}
