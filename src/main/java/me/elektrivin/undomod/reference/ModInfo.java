package me.elektrivin.undomod.reference;

public class ModInfo {
    public static final String MOD_ID = "undomod";
    public static final String VERSION = "0.1";
    public static final String PROXY_CLIENT = "me.elektrivin.undomod.proxy.ClientProxy";
    public static final String PROXY_SERVER = "me.elektrivin.undomod.proxy.ServerProxy";
}
