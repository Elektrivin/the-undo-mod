package me.elektrivin.undomod.reference;

import me.elektrivin.undomod.helper.BlockHelper;
import me.elektrivin.undomod.helper.ChatHelper;
import me.elektrivin.undomod.helper.ItemHelper;
import net.minecraft.block.BlockChest;
import net.minecraft.block.BlockFurnace;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Tuple;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;
import sun.security.krb5.Config;

import javax.swing.plaf.basic.BasicComboBoxUI;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class BuildAction {
    public enum Type {
        PLACE,
        BREAK
    }

    private Date timestamp;
    private EntityPlayer player;
    private IBlockState block;
    private Type type;
    private Position position;
    private ArrayList<Tuple<ItemStack, Integer>> drops;
    private ArrayList<ItemStack> inventory;
    private int xp;

    public BuildAction(EntityPlayer player, IBlockState block, Type type, Position position) {
        this.timestamp = new Date();
        this.player = player;
        this.block = block;
        this.type = type;
        this.position = position;
        this.drops = new ArrayList<>();
        this.inventory = new ArrayList<>();
    }

    public BuildAction(EntityPlayer player, IBlockState block, Type type, Position position, ArrayList<Tuple<ItemStack, Integer>> drops, int xp) {
        this(player, block, type, position);
        this.drops = drops;
        this.xp = xp;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public EntityPlayer getPlayer() {
        return player;
    }

    public void setPlayer(EntityPlayer player) {
        this.player = player;
    }

    public IBlockState getBlock() {
        return block;
    }

    public void setBlock(IBlockState block) {
        this.block = block;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public ArrayList<Tuple<ItemStack, Integer>> getDrops() {
        return drops;
    }

    public void setDrops(ArrayList<Tuple<ItemStack, Integer>> drops) {
        this.drops = drops;
    }

    public ArrayList<ItemStack> getInventory() { return this.inventory; }

    public void setInventory(ArrayList<ItemStack> inventory) { this.inventory = inventory; }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public int getDropCount() {
        int totalDrops = 0;

        for (Tuple<ItemStack, Integer> drop : drops) {
            totalDrops += drop.getSecond();
        }

        return totalDrops;
    }

    private boolean performUndoRedo(boolean doUndo) {

        boolean doPlace = ((type == Type.BREAK && doUndo) || (type == Type.PLACE && !doUndo));

        player.addExperience((doUndo ? xp * -1 : xp));

        if(doPlace)
            return performPlace(doUndo);
        else
            return performBreak(doUndo);
    }

    private boolean performPlace(boolean doUndo) {

        //TODO: make option for keeping track of list of items instead of requiring them in the inventory? Then close entities should be removed as well.
        //TODO: fill inventory if there was an inventory (add to buildAction!)

        for (Tuple<ItemStack, Integer> drop : drops) {
            if(!player.inventory.hasItemStack(ItemHelper.getCopy(drop.getFirst(), drop.getSecond()))) {
                ChatHelper.sendErrorMessage(player, "messages.error.not_enough_items_" + (doUndo ? "undo" : "redo"));
                return false;
            }
        }

        if(inventory.size() > 0)
        {
            for(ItemStack inventoryItem : inventory)
                if(inventoryItem.getItem() != Items.AIR && !player.inventory.hasItemStack(inventoryItem)) {
                    ChatHelper.sendErrorMessage(player, "messages.error.not_enough_items_" + (doUndo ? "undo" : "redo"));
                    return false;

                }
        }

        for (Tuple<ItemStack, Integer> drop : drops) {
            player.inventory.clearMatchingItems(drop.getFirst().getItem(), drop.getFirst().getMetadata(), drop.getSecond(), drop.getFirst().getTagCompound());
        }

        BlockHelper.PlaceBlock(position, block);

        if(inventory.size() > 0)
        {
            TileEntity tile = position.getWorld().getTileEntity(position.getPosition());
            if(tile != null)
            {
                IItemHandler itemHandler = tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
                if(itemHandler != null)
                    for(int i = 0; i < itemHandler.getSlots(); i++) {
                        ItemStack currentItem = inventory.get(i);
                        itemHandler.insertItem(i, currentItem, false);
                        player.inventory.clearMatchingItems(currentItem.getItem(), currentItem.getMetadata(), currentItem.getCount(), currentItem.getTagCompound());
                    }
            }
        }

        return true;
    }

    private boolean performBreak(boolean doUndo) {

        for (Tuple<ItemStack, Integer> drop : drops) {
            ItemStack currentStack = ItemHelper.getCopy(drop.getFirst(), drop.getSecond());
            if(Configurations.undoRedo.dropRefund)
                ItemHelper.tryDropInWorld(player, position, currentStack);
            else
                ItemHandlerHelper.giveItemToPlayer(player, currentStack);
        }

        BlockHelper.BreakBlock(position);

        return true;
    }

    public boolean undo() {

        if(Configurations.undoRedo.enableRange && new Position(player).getDistance(position) > Configurations.undoRedo.range) {
            ChatHelper.sendErrorMessage(player, "messages.error.not_in_range_undo");
            return false;
        }

        return performUndoRedo(true);

    }

    public boolean redo() {
        if(Configurations.undoRedo.enableRange && new Position(player).getDistance(position) > Configurations.undoRedo.range) {
            ChatHelper.sendErrorMessage(player, "messages.error.not_in_range_redo");
            return false;
        }

        return performUndoRedo(false);
    }

    @Override
    public String toString() {
        switch (this.type) {
            case BREAK:
                return player.getName() + " broke block: " + block.getBlock().getLocalizedName() + " at " + position + " and received " + getDropCount() + " drops and " + xp + " xp.";
            case PLACE:
                return player.getName() + " placed block: " + block.getBlock().getLocalizedName() + " at " + position + " and used " + getDropCount() + " items.";
            default:
                return null;
        }
    }
}
