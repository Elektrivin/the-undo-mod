package me.elektrivin.undomod.reference;

public class Messages {

    public static class Info {

    }

    public static class Error {

        public final static String NOTHING_TO_UNDO = "messages.error.nothing_to_undo";
        public final static String NOTHING_TO_REDO = "messages.error.nothing_to_redo";

    }

}
