package me.elektrivin.undomod.proxy;

import me.elektrivin.undomod.client.settings.KeyBindings;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ClientProxy extends CommonProxy {

    @Override
    public void onPreInit(FMLPreInitializationEvent event) {
        super.onPreInit(event);

        KeyBindings.init();
    }
}
