package me.elektrivin.undomod.proxy;

import me.elektrivin.undomod.controller.HistoryStackController;
import me.elektrivin.undomod.init.ModHandlers;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class CommonProxy implements IProxy {

    public void onPreInit(FMLPreInitializationEvent event) {

    }

    public void onInit(FMLInitializationEvent event) {
        ModHandlers.init();
    }

    public void onPostInit(FMLPostInitializationEvent event) {
        HistoryStackController.init();
    }

}
