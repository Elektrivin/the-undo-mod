package me.elektrivin.undomod.helper;

import me.elektrivin.undomod.reference.Position;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraftforge.items.ItemHandlerHelper;

public class ItemHelper {

    public static EntityItem getEntityItem(Position position, ItemStack itemStack) {
        return new EntityItem(position.getWorld(), position.getPosition().getX()+.5, position.getPosition().getY()+1, position.getPosition().getZ()+0.5, itemStack);
    }

    public static boolean dropInWorld(Position position, ItemStack itemStack) {
        return position.getWorld().isBlockLoaded(position.getPosition()) && position.getWorld().spawnEntity(getEntityItem(position, itemStack));
    }

    public static void tryDropInWorld(EntityPlayer player, Position position, ItemStack itemStack)
    {
        if(!dropInWorld(position, itemStack))
            if(!dropInWorld(new Position(player.getPosition(), player.getEntityWorld()), itemStack))
                ItemHandlerHelper.giveItemToPlayer(player, itemStack);
    }

    public static ItemStack getFromPlayerHand(EntityPlayer player, EnumHand hand) {
        return player.getHeldItem(hand).copy();
    }

    public static ItemStack getCopy(ItemStack stack, int amount) {
        return new ItemStack(stack.getItem(), amount, stack.getMetadata(), stack.getTagCompound());
    }
}
