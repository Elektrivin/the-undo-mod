package me.elektrivin.undomod.helper;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.text.TextComponentBase;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

public class ChatHelper {

    public static TextFormatting defaultTextColor = TextFormatting.YELLOW;
    public static TextFormatting errorTextColor = TextFormatting.RED;

    public static void sendMessage(ICommandSender sender, String message, TextFormatting color, boolean localized) {
        TextComponentBase messageComponent = (localized ? new TextComponentTranslation(message) : new TextComponentString(message));
        messageComponent.getStyle().setColor(color);
        sender.sendMessage(messageComponent);
    }

    public static void sendMessage(ICommandSender sender, String message, boolean localized) {
        sendMessage(sender, message, defaultTextColor, localized);
    }

    public static void sendMessage(ICommandSender sender, String message) {
        sendMessage(sender, message, defaultTextColor, true);
    }

    public static void sendErrorMessage(ICommandSender sender, String message, boolean localized) {
        sendMessage(sender, message, errorTextColor, localized);
    }

    public static void sendErrorMessage(ICommandSender sender, String message) {
        sendMessage(sender, message, errorTextColor, true);
    }

}
