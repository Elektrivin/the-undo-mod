package me.elektrivin.undomod.helper;

import me.elektrivin.undomod.reference.Position;
import net.minecraft.block.state.IBlockState;

public class BlockHelper {

    public static void PlaceBlock(Position position, IBlockState block) {
        position.getWorld().setBlockState(position.getPosition(), block);
    }

    public static void BreakBlock(Position position) {
        //TODO: add non silent mode
        //TODO: check for multiblock (bed f.i.)
        position.getWorld().setBlockToAir(position.getPosition());
    }

}
