package me.elektrivin.undomod.client.handler;

import me.elektrivin.undomod.client.settings.KeyBindings;
import me.elektrivin.undomod.controller.HistoryStackController;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class KeyInputHandler {

    @SubscribeEvent
    public void handleKeyInputEvent(InputEvent.KeyInputEvent event) {

        if (FMLClientHandler.instance().getClient().inGameHasFocus && FMLClientHandler.instance().getClientPlayerEntity() != null) {

            EntityPlayer entityPlayer = FMLClientHandler.instance().getClientPlayerEntity();

            if (entityPlayer.getEntityWorld().isRemote) {

                if(KeyBindings.UNDO.isPressed()) {
                    HistoryStackController.undo(entityPlayer);
                } else if (KeyBindings.REDO.isPressed()) {
                    HistoryStackController.redo(entityPlayer);
                }

            }
        }

    }
}
