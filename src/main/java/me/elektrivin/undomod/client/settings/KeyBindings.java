package me.elektrivin.undomod.client.settings;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;

@SideOnly(Side.CLIENT)
public class KeyBindings {

    private static final String category = "The Undo Mod";

    public static final KeyBinding UNDO = new KeyBinding("Undo", Keyboard.KEY_Z, category);
    public static final KeyBinding REDO = new KeyBinding("Redo", Keyboard.KEY_Y, category);

    public static void init() {
        ClientRegistry.registerKeyBinding(UNDO);
        ClientRegistry.registerKeyBinding(REDO);
    }

}
