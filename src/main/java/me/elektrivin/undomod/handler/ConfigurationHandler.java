package me.elektrivin.undomod.handler;

import me.elektrivin.undomod.reference.ModInfo;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ConfigurationHandler {

    @SubscribeEvent
    public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event) {
        if (event.getModID().equalsIgnoreCase(ModInfo.MOD_ID)) {
            ConfigManager.load(ModInfo.MOD_ID, Config.Type.INSTANCE);
            System.out.println("Configurations updated!");
        }
    }
}
