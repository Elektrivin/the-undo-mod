package me.elektrivin.undomod.handler;

import me.elektrivin.undomod.controller.HistoryStackController;
import me.elektrivin.undomod.helper.ItemHelper;
import me.elektrivin.undomod.reference.BuildAction;
import me.elektrivin.undomod.reference.Position;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Tuple;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

import java.util.ArrayList;

public class BuildActionHandler {

    private BuildAction currentBuildAction;

    @SubscribeEvent
    public void onBlockPlace(BlockEvent.PlaceEvent event) {
        //TODO: check how much xp the player has before placing the block and after placing the block and save the difference.
        //TODO: check inventory before and after placing and keep track of all items used for placing.
        ArrayList<Tuple<ItemStack, Integer>> itemCost =  new ArrayList<>();
        itemCost.add(new Tuple<>(ItemHelper.getFromPlayerHand(event.getPlayer(), event.getHand()), 1));

        currentBuildAction = new BuildAction(event.getPlayer(), event.getPlacedBlock(), BuildAction.Type.PLACE, new Position(event.getPos(), event.getWorld()), itemCost,0);
        HistoryStackController.push(currentBuildAction);
    }

    @SubscribeEvent
    public void onMultiBlockPlace(BlockEvent.MultiPlaceEvent event) {
        //TODO: catch?
    }

    @SubscribeEvent
    public void onBlockBreak(BlockEvent.BreakEvent event) {

        currentBuildAction = new BuildAction(event.getPlayer(), event.getState(), BuildAction.Type.BREAK, new Position(event.getPos(), event.getWorld()));

        TileEntity tile = event.getWorld().getTileEntity(event.getPos());
        if(tile != null)
        {
            IItemHandler itemHandler = tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
            if(itemHandler != null)
                for(int i = 0; i < itemHandler.getSlots(); i++)
                    currentBuildAction.getInventory().add(itemHandler.getStackInSlot(i).copy());
        }

        currentBuildAction.setXp(event.getExpToDrop());

        HistoryStackController.push(currentBuildAction);
    }

    @SubscribeEvent
    public void onBlockHarvestDrops(BlockEvent.HarvestDropsEvent event) {
        for(ItemStack stack : event.getDrops()) {
            currentBuildAction.getDrops().add(new Tuple<>(stack, stack.getCount()));
        }

        HistoryStackController.popPush(currentBuildAction);
    }

}
