package me.elektrivin.undomod.init;

import me.elektrivin.undomod.client.handler.KeyInputHandler;
import me.elektrivin.undomod.handler.BuildActionHandler;
import me.elektrivin.undomod.handler.ConfigurationHandler;
import net.minecraftforge.common.MinecraftForge;

public class ModHandlers {

    public static void init() {
        MinecraftForge.EVENT_BUS.register(new ConfigurationHandler());
        MinecraftForge.EVENT_BUS.register(new BuildActionHandler());
        MinecraftForge.EVENT_BUS.register(new KeyInputHandler());
    }

}
