package me.elektrivin.undomod.controller;

import me.elektrivin.undomod.reference.Configurations;
import me.elektrivin.undomod.helper.ChatHelper;
import me.elektrivin.undomod.reference.BuildAction;
import me.elektrivin.undomod.reference.Messages;
import net.minecraft.entity.player.EntityPlayer;

import java.util.HashMap;
import java.util.Stack;
import java.util.UUID;

public class HistoryStackController {

    private static HashMap<UUID, Stack<BuildAction>> stackMap;
    private static HashMap<UUID, Integer> offsetMap;

    public static void init() {
        stackMap = new HashMap<>();
        offsetMap = new HashMap<>();
    }

    public static void push(BuildAction action) {
        UUID playerUUID = action.getPlayer().getUniqueID();

        if(!stackMap.containsKey(playerUUID))
            stackMap.put(playerUUID, new Stack<>());

        if(!offsetMap.containsKey(playerUUID))
            offsetMap.put(playerUUID, 0);

        for(int i = offsetMap.get(playerUUID); i > 0; i--) {
            stackMap.get(playerUUID).pop();
        }

        offsetMap.replace(playerUUID, 0);

        if(stackMap.get(playerUUID).size() == Configurations.undoRedo.historyStackSize){
            stackMap.get(playerUUID).removeElementAt(0);
        }

        stackMap.get(playerUUID).push(action);
    }

    public static void popPush(BuildAction action) {
        UUID playerUUID = action.getPlayer().getUniqueID();

        stackMap.get(playerUUID).pop();
        stackMap.get(playerUUID).push(action);
    }

    public static void undo(EntityPlayer player) {
        if(!offsetMap.containsKey(player.getUniqueID()) || offsetMap.get(player.getUniqueID()) == stackMap.get(player.getUniqueID()).size()) {
            ChatHelper.sendErrorMessage(player, Messages.Error.NOTHING_TO_UNDO);
            return;
        }

        if(stackMap.get(player.getUniqueID()).elementAt(stackMap.get(player.getUniqueID()).size() - offsetMap.get(player.getUniqueID()) - 1).undo())
            offsetMap.replace(player.getUniqueID(), offsetMap.get(player.getUniqueID()) + 1);
    }

    public static void redo(EntityPlayer player) {
        if(!offsetMap.containsKey(player.getUniqueID()) || offsetMap.get(player.getUniqueID()) == 0) {
            ChatHelper.sendErrorMessage(player, Messages.Error.NOTHING_TO_REDO);
            return;
        }

        if(stackMap.get(player.getUniqueID()).elementAt(stackMap.get(player.getUniqueID()).size() - offsetMap.get(player.getUniqueID())).redo())
            offsetMap.replace(player.getUniqueID(), offsetMap.get(player.getUniqueID()) - 1);

    }

}
